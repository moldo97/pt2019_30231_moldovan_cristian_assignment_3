package ps.BusinessLogic;

import ps.Model.Client;

import java.util.List;

public interface ClientService {
    Client findClientById(Long id);

    List<Client> findAllClients();

    Client createClient(Client client);

    Client updateClient(Client client);

    boolean deleteClientById(Long id);
}
