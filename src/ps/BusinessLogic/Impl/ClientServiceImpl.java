package ps.BusinessLogic.Impl;

import ps.Model.Client;
import ps.BusinessLogic.ClientService;
import ps.DataAccess.ClientRepository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ClientServiceImpl implements ClientService {
    ClientRepository clientRepository;

    public ClientServiceImpl(ClientRepository clientRepository){
        this.clientRepository = clientRepository;
    }

    @Override
    public Client findClientById(Long id) {
        return clientRepository.findById(id);
    }

    @Override
    public List<Client> findAllClients() {
        List<Client> clients = new ArrayList<>();
        clients = clientRepository.findAll();
        clients.sort(Comparator.comparing(client -> client.getName()));
        return clients;
    }

    @Override
    public Client createClient(Client client) {
        return clientRepository.create(client);
    }

    @Override
    public Client updateClient(Client client) {
        if(client.getId() != null) {
            return clientRepository.update(client);
        }
        return null;
    }

    @Override
    public boolean deleteClientById(Long id) {
        Client client = clientRepository.findById(id);
        if(client.getId() != null) {
            return clientRepository.deleteById(client.getId());
        }
        return false;
    }
}
