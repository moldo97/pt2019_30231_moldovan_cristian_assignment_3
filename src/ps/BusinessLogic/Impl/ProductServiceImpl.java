package ps.BusinessLogic.Impl;

import ps.Model.Product;
import ps.BusinessLogic.ProductService;
import ps.DataAccess.ProductRepository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ProductServiceImpl implements ProductService {
    ProductRepository productRepository;

    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public Product findProductById(Long id) {
        return productRepository.findById(id);
    }

    @Override
    public List<Product> findAllProducts() {
        List<Product> products = new ArrayList<>();
        products = productRepository.findAll();
        products.sort(Comparator.comparing(product -> product.getName()));
        return products;
    }

    @Override
    public Product createProduct(Product product) {
        return productRepository.create(product);
    }

    @Override
    public Product updateProduct(Product product) {
        if(product.getId()!=null){
            return productRepository.update(product);
        }
        return null;
    }

    @Override
    public boolean deleteProductById(Long id) {
        Product product = productRepository.findById(id);
        if(product.getId()!=null) {
            return productRepository.deleteById(product.getId());
        }
        return false;
    }
}
