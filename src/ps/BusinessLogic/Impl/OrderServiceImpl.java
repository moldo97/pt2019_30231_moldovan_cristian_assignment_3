package ps.BusinessLogic.Impl;

import ps.Model.Order;
import ps.BusinessLogic.OrderService;
import ps.DataAccess.OrderRepository;

import java.util.Comparator;
import java.util.List;

public class OrderServiceImpl implements OrderService {
    OrderRepository orderRepository;

    public OrderServiceImpl(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @Override
    public Order createOrder(Order order) {
        return orderRepository.create(order);
    }

    @Override
    public Order findOrderById(Long id) {
        return orderRepository.findById(id);
    }

    @Override
    public List<Order> findAllOrders() {
        List<Order> orders = orderRepository.findAll();
        orders.sort(Comparator.comparing(order -> order.getId_client()));
        return orders;
    }

    @Override
    public boolean deleteOrderById(Long id) {
        Order order = orderRepository.findById(id);
        if (order.getId() != null) {
            orderRepository.deleteById(order.getId());
        }
        return false;
    }
}
