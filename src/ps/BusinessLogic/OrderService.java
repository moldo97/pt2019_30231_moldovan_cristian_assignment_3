package ps.BusinessLogic;

import ps.Model.Order;

import java.util.List;

public interface OrderService {
    Order createOrder(Order order);

    Order findOrderById(Long id);

    List<Order> findAllOrders();

    boolean deleteOrderById(Long id);
}
