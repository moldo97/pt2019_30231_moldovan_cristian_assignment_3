package ps.BusinessLogic;

import ps.Model.Product;

import java.util.List;

public interface ProductService {
    Product findProductById(Long id);

    List<Product> findAllProducts();

    Product createProduct(Product product);

    Product updateProduct(Product product);

    boolean deleteProductById(Long id);
}
