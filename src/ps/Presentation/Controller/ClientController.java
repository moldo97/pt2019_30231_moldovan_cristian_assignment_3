package ps.Presentation.Controller;

import ps.Main;
import ps.Model.Client;
import ps.Presentation.View.ClientView;
import ps.Utils.DataConverter;
import ps.BusinessLogic.ClientService;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class ClientController {
    private final ClientView clientView;
    private final DataConverter dataConverter;
    private final ClientService clientService;

    public ClientController(ClientView clientView, DataConverter dataConverter, ClientService clientService) {
        this.clientView = clientView;
        this.dataConverter = dataConverter;
        this.clientService = clientService;

        //collect initial data
        List<Client> clients = clientService.findAllClients();

        Object[][] clientData = this.dataConverter.clientToTableData(clients);
        String[] clientColumnNames = this.dataConverter.clientToTableColumnNames();


        //set data
        this.clientView.refreshClientTable(clientData,clientColumnNames);

        //add action listeners
        this.clientView.addExitActionListener(new ExitActionListener());
        this.clientView.addCreateActionListeners(new CreateActionListener());
        this.clientView.addUpdateActionListener(new UpdateActionListener());
        this.clientView.addDeleteActionListener(new DeleteActionListener());

        this.clientView.setVisible(true);
    }

    private class ExitActionListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            clientView.setVisible(false);
            Main.openInit();
        }
    }

    private class CreateActionListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            String name = clientView.getName();
            String adress = clientView.getAdress();

            Client client = new Client();
            client.setAdress(adress);
            client.setName(name);

            if(client.getName().equals("")){
                writeMessage("Enter a name");
            }
            else if(client.getAdress().equals("")){
                writeMessage("Enter an adress");
            }
            else{
                clientService.createClient(client);
                refreshSelectedClientTable();
            }
        }
    }

    private class UpdateActionListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            String name = clientView.getName();
            String adress = clientView.getAdress();
            String idText = clientView.getIdTextField();

            if(idText.equals("")){
                writeMessage("Enter an id");
            }
            else if(name.equals("")){
                writeMessage("Enter a name");
            }
            else if(adress.equals("")){
                writeMessage("Enter an adress");
            }
            else{
                Long id = clientView.getId();

                Client client = clientService.findClientById(id);
                if(client == null){
                    writeMessage("Client does not exist");
                }
                else{
                    client.setName(name);
                    client.setAdress(adress);

                    clientService.updateClient(client);
                    refreshSelectedClientTable();
                }
            }
        }
    }

    private class DeleteActionListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            Long id = clientView.getSelectedClientId();
            clientService.deleteClientById(id);
            refreshSelectedClientTable();
        }
    }

    private void refreshSelectedClientTable(){
        List<Client> clients = clientService.findAllClients();

        Object[][] clientData = dataConverter.clientToTableData(clients);
        String[] clientColumnNames = dataConverter.clientToTableColumnNames();

        clientView.refreshClientTable(clientData,clientColumnNames);
    }

    private void writeMessage(String message){
        JOptionPane.showMessageDialog(clientView,message);
    }
}
