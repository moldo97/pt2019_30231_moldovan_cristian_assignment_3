package ps.Presentation.Controller;

import ps.BusinessLogic.ProductService;
import ps.Main;
import ps.Model.Client;
import ps.Model.Order;
import ps.Model.Product;
import ps.Presentation.View.ClientView;
import ps.Presentation.View.OrderView;
import ps.BusinessLogic.OrderService;
import ps.Presentation.View.ProductView;
import ps.Utils.DataConverter;
import ps.Utils.Impl.DataConverterImpl;
import ps.Utils.Impl.ReflectionImpl;
import ps.Utils.Impl.ReportPDFImpl;
import ps.Utils.Reflection;
import ps.Utils.ReportPDF;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

public class OrderController {
    private final OrderView orderView;
    private final ProductView productView;
    private final ClientView clientView;

    private static Reflection reflection;
    private static ReportPDF reportPDF;
    private static DataConverter dataConverter;


    private final OrderService orderService;
    private final ProductService productService;


    public OrderController(OrderView orderView, ProductView productView, ClientView clientView, OrderService orderService, ProductService productService, Reflection reflection) {
        this.orderView = orderView;
        this.productView = productView;
        this.clientView = clientView;

        this.orderService = orderService;
        this.productService = productService;

        this.reflection = reflection;

        //collect initial data
        refreshReflectionTable();

        //add action listeners
        this.orderView.addExitActionListener(new ExitActionListener());
        this.orderView.addDeleteActionListener(new DeleteActionListener());
        this.orderView.addOrderActionListener(new OrderActionListener());
        this.orderView.addReportActionListener(new ReportActionListener());

        this.orderView.setVisible(true);
    }

    private void refreshReflectionTable(){
        DefaultTableModel model = new DefaultTableModel();
        List<Order> orders = orderService.findAllOrders();

        model.setColumnIdentifiers(this.reflection.getColumnNames(orders.get(0)));
        this.reflection.addRows(model,orders);

        model.fireTableDataChanged();

        orderView.getOrderTable().setModel(model);
    }

    public class ExitActionListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            orderView.setVisible(false);
            Main.openInit();
        }
    }

    public class DeleteActionListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            Long id = orderView.getSelectedOrderId();
            orderService.deleteOrderById(id);
            refreshReflectionTable();
        }
    }

    public class OrderActionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            Long idClient = clientView.getSelectedClientId();
            Long idProdus = productView.getSelectedProductId();
            String quantityText = orderView.getQuantityText();

            if (quantityText.equals("")) {
                writeMessage("Enter a quantity");
            } else {
                Integer quantity = orderView.getQuantity();
                if (quantity <= 0) {
                    writeMessage("Enter a positive quantity");
                } else {
                    Product product = productService.findProductById(idProdus);
                    if (quantity > product.getQuantity()) {
                        writeMessage("Under stock");
                    } else {
                        Order order = new Order();
                        order.setId_client(idClient);
                        order.setId_produs(idProdus);
                        order.setCantitate(quantity);

                        product.setQuantity(product.getQuantity() - quantity);
                        productService.updateProduct(product);

                        orderService.createOrder(order);
                        refreshSelectedProductTable();
                        refreshReflectionTable();
                    }
                }
            }
        }
    }

        public class ReportActionListener implements ActionListener {

            @Override
            public void actionPerformed(ActionEvent e) {
                reportPDF = new ReportPDFImpl();
                Long id = orderView.getSelectedOrderId();
                Order order = orderService.findOrderById(id);

                List<Order> orders = orderService.findAllOrders();
                List<Product> products = new ArrayList<>();

                orders.forEach(order1 -> {
                    if (order1.getId_client() == order.getId_client()) {
                        Product product = productService.findProductById(order1.getId_produs());
                        products.add(product);
                    }
                });

                reportPDF.exportReport(order.getId_client(), products);
            }
        }

    private void writeMessage(String message){
        JOptionPane.showMessageDialog(orderView,message);
    }

    private void refreshSelectedProductTable(){
        List<Product> products = productService.findAllProducts();
        dataConverter = new DataConverterImpl();

        Object[][] productData = dataConverter.productToTableData(products);
        String[] productColumnNames = dataConverter.productToTableColumnNames();

        productView.refreshProductTable(productData,productColumnNames);
    }
}
