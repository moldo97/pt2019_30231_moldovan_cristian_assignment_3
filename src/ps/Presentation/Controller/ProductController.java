package ps.Presentation.Controller;

import ps.Main;
import ps.Model.Product;
import ps.Presentation.View.ProductView;
import ps.BusinessLogic.ProductService;
import ps.Utils.DataConverter;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class ProductController {
    private final ProductView productView;
    private final DataConverter dataConverter;
    private final ProductService productService;

    public ProductController(ProductView productView, DataConverter dataConverter, ProductService productService) {
        this.productView = productView;
        this.dataConverter = dataConverter;
        this.productService = productService;

        //collect initial data
        List<Product> products = productService.findAllProducts();

        Object[][] productData = this.dataConverter.productToTableData(products);
        String[] productColumnNames = this.dataConverter.productToTableColumnNames();

        //set data
        this.productView.refreshProductTable(productData,productColumnNames);

        //add action listeners
        this.productView.addExitActionListener(new ExitActionListener());
        this.productView.addCreateActionListeners(new CreateActionListener());
        this.productView.addUpdateActionListener(new UpdateActionListener());
        this.productView.addDeleteActionListener(new DeleteActionListener());

        this.productView.setVisible(true);
    }

    private class ExitActionListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            productView.setVisible(false);
            Main.openInit();
        }
    }

    private class CreateActionListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            String name = productView.getName();
            String priceText = productView.getPriceText();
            String quantityText = productView.getQuantityText();

            if(name.equals("")){
                writeMessage("Enter a name");
            }
            else if(priceText.equals("")){
                writeMessage("Enter a price");
            }
            else if(quantityText.equals("")){
                writeMessage("Enter a quantity");
            }
            else{
                Integer price = productView.getPrice();
                Integer quantity = productView.getQuantity();

                if(price <= 0){
                    writeMessage("Enter a positive price");
                }
                else if(quantity <= 0){
                    writeMessage("Enter a positive quantity");
                }
                else{
                    Product product = new Product();
                    product.setName(name);
                    product.setPrice(price);
                    product.setQuantity(quantity);

                    productService.createProduct(product);
                    refreshSelectedProductTable();
                }
            }
        }
    }

    private class UpdateActionListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            String name = productView.getName();
            String priceText = productView.getPriceText();
            String quantityText = productView.getQuantityText();
            String idText = productView.getIdText();

            if(idText.equals("")){
                writeMessage("Enter an id");
            }
            else if(name.equals("")){
                writeMessage("Enter a name");
            }
            else if(priceText.equals("")){
                writeMessage("Enter a price");
            }
            else if(quantityText.equals("")){
                writeMessage("Enter a quantity");
            }
            else{
                Integer price = productView.getPrice();
                Integer quantity = productView.getQuantity();
                Long id = productView.getId();

                if(price <= 0){
                    writeMessage("Enter a positive price");
                }
                else if(quantity <= 0){
                    writeMessage("Enter a positive quantity");
                }
                else{
                    Product product = productService.findProductById(id);

                    if(product == null){
                        writeMessage("Product does not exist");
                    }
                    else {
                        product.setName(name);
                        product.setPrice(price);
                        product.setQuantity(quantity);

                        productService.updateProduct(product);
                        refreshSelectedProductTable();
                    }
                }
            }
        }
    }

    private class DeleteActionListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            Long id = productView.getSelectedProductId();
            productService.deleteProductById(id);
            refreshSelectedProductTable();
        }
    }

    private void writeMessage(String message){
        JOptionPane.showMessageDialog(productView,message);
    }

    private void refreshSelectedProductTable(){
        List<Product> products = productService.findAllProducts();

        Object[][] productData = this.dataConverter.productToTableData(products);
        String[] productColumnNames = this.dataConverter.productToTableColumnNames();

        productView.refreshProductTable(productData,productColumnNames);
    }
}
