package ps.Presentation.Controller;

import ps.Main;
import ps.Presentation.View.InitView;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class InitController {
    private final InitView initView;

    public InitController(InitView initView){
        this.initView = initView;

        //add action listeners
        this.initView.addClientsLoginActionListener(new ClientsActionListener());
        this.initView.addProductsLoginActionListener(new ProductsActionListener());
        this.initView.addOrdersLoginActionListener(new OrdersActionListener());

        this.initView.setVisible(true);
    }

    private class ClientsActionListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            Main.openClientView();
        }
    }

    private class ProductsActionListener implements  ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            Main.openProductView();
        }
    }

    private class OrdersActionListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {

            Main.openOrderView();
        }
    }
}
