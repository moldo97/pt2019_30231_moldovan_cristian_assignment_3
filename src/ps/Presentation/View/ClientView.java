package ps.Presentation.View;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionListener;

public class ClientView extends JFrame {
    private static final String TITLE = "Clients View";
    private static final int WIDTH = 700;
    private static final int HEIGHT = 600;

    private final JLabel nameLabel;
    private final JLabel adressLabel;
    private final JLabel idLabel;

    private final JButton create;
    private final JButton delete;
    private final JButton update;
    private final JButton exit;

    private JTextField idTextField = new JTextField();
    private JTextField nameTextField = new JTextField();
    private JTextField adressTextField = new JTextField();

    private final JTable  clientsTable;

    private final JPanel left;
    private final JPanel right;
    private final JPanel buttons;

    public ClientView() throws HeadlessException {
        super(TITLE);
        nameLabel = new JLabel("Name");
        adressLabel = new JLabel("Adress");
        idLabel = new JLabel("Id");

        create = new JButton("Create Client");
        delete = new JButton("Delete Client");
        update = new JButton("Update Client");
        exit = new JButton("Exit");

        clientsTable = new JTable();

        left = new JPanel();
        right = new JPanel();
        buttons = new JPanel();

        initializeView();
        setVisible(false);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    private void initializeView() {

        left.setLayout(new BoxLayout(left, BoxLayout.PAGE_AXIS));
        right.setLayout(new BoxLayout(right,BoxLayout.PAGE_AXIS));
        buttons.setLayout(new BoxLayout(buttons,BoxLayout.PAGE_AXIS));
        setLayout(new BoxLayout(getContentPane(), BoxLayout.LINE_AXIS));


        left.add(Box.createRigidArea(new Dimension(20, 20)));
        JScrollPane jScrollPaneArticles = new JScrollPane(clientsTable, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPaneArticles.setBorder(new EmptyBorder(10, 10, 10, 10));
        left.add(jScrollPaneArticles);

        add(left);

        right.add(Box.createRigidArea(new Dimension(20,20)));
        idLabel.setBorder(new EmptyBorder(10,10,10,10));
        right.add(idLabel);
        right.add(Box.createRigidArea(new Dimension(20,20)));
        idTextField.setMaximumSize(new Dimension(Integer.MAX_VALUE, idTextField.getPreferredSize().height) );
        right.add(idTextField);

        right.add(Box.createRigidArea(new Dimension(20,20)));
        nameLabel.setBorder(new EmptyBorder(10,10,10,10));
        right.add(nameLabel);
        right.add(Box.createRigidArea(new Dimension(20,20)));
        nameTextField.setMaximumSize(new Dimension(Integer.MAX_VALUE, nameTextField.getPreferredSize().height) );
        right.add(nameTextField);

        right.add(Box.createRigidArea(new Dimension(20,20)));
        adressLabel.setBorder(new EmptyBorder(10,10,10,10));
        right.add(adressLabel);
        right.add(Box.createRigidArea(new Dimension(20,20)));
        adressTextField.setMaximumSize(new Dimension(Integer.MAX_VALUE, adressTextField.getPreferredSize().height) );
        right.add(adressTextField);

        add(right);

        buttons.add(Box.createRigidArea(new Dimension(50, 50)));
        create.setBorder(new EmptyBorder(10, 10, 10, 10));
        buttons.add(create);

        buttons.add(Box.createRigidArea(new Dimension(50, 50)));
        update.setBorder(new EmptyBorder(10, 10, 10, 10));
        buttons.add(update);

        buttons.add(Box.createRigidArea(new Dimension(50, 50)));
        delete.setBorder(new EmptyBorder(10, 10, 10, 10));
        buttons.add(delete);

        buttons.add(Box.createRigidArea(new Dimension(50, 50)));
        exit.setBorder(new EmptyBorder(10, 10, 10, 10));
        buttons.add(exit);

        add(buttons);

        setSize(WIDTH, HEIGHT);
        setLocationRelativeTo(null);
    }

    //Get-ere pentru controller
    public String getName() { return this.nameTextField.getText(); }

    public String getAdress() { return this.adressTextField.getText(); }

    public Long getId() { return Long.parseLong(this.idTextField.getText()); }

    public String getIdTextField() { return this.idTextField.getText(); }

    public Long getSelectedClientId(){
        return (Long) clientsTable.getValueAt(clientsTable.getSelectedRow(),0);
    }

    //Refresh la tabel
    public void refreshClientTable(Object[][] data, String[] columnNames){
        DefaultTableModel tableModel = (DefaultTableModel) clientsTable.getModel();
        tableModel.setDataVector(data,columnNames);
        tableModel.setColumnIdentifiers(columnNames);
        tableModel.fireTableDataChanged();
    }

    //action listeners pentru controller
    public void addCreateActionListeners(ActionListener actionListener){
        create.addActionListener(actionListener);
    }

    public void addUpdateActionListener(ActionListener actionListener){
        update.addActionListener(actionListener);
    }

    public void addDeleteActionListener(ActionListener actionListener){
        delete.addActionListener(actionListener);
    }

    public void addExitActionListener(ActionListener actionListener){
        exit.addActionListener(actionListener);
    }
}
