package ps.Presentation.View;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionListener;

public class ProductView extends JFrame {
    private static final String TITLE = "Products View";
    private static final int WIDTH = 700;
    private static final int HEIGHT = 600;

    private final JLabel nameLabel;
    private final JLabel priceLabel;
    private final JLabel quantityLabel;
    private final JLabel idLabel;

    private final JButton create;
    private final JButton delete;
    private final JButton update;
    private final JButton exit;

    private JTextField nameTextField = new JTextField();
    private JTextField priceTextField = new JTextField();
    private JTextField quantityTextField = new JTextField();
    private JTextField idTextField = new JTextField();

    private final JTable  productsTable;

    private final JPanel left;
    private final JPanel right;
    private final JPanel buttons;

    public ProductView() throws HeadlessException {
        super(TITLE);
        nameLabel = new JLabel("Name");
        priceLabel = new JLabel("Price");
        quantityLabel= new JLabel("Quantity");
        idLabel = new JLabel("Id");

        create = new JButton("Create Product");
        delete = new JButton("Delete Product");
        update = new JButton("Update Product");
        exit = new JButton("Exit");

        productsTable = new JTable();

        left = new JPanel();
        right = new JPanel();
        buttons = new JPanel();

        initializeView();
        setVisible(false);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    private void initializeView() {

        left.setLayout(new BoxLayout(left, BoxLayout.PAGE_AXIS));
        right.setLayout(new BoxLayout(right,BoxLayout.PAGE_AXIS));
        buttons.setLayout(new BoxLayout(buttons,BoxLayout.PAGE_AXIS));
        setLayout(new BoxLayout(getContentPane(), BoxLayout.LINE_AXIS));


        left.add(Box.createRigidArea(new Dimension(20, 20)));
        JScrollPane jScrollPaneArticles = new JScrollPane(productsTable, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPaneArticles.setBorder(new EmptyBorder(10, 10, 10, 10));
        left.add(jScrollPaneArticles);

        add(left);

        right.add(Box.createRigidArea(new Dimension(20,20)));
        idLabel.setBorder(new EmptyBorder(10,10,10,10));
        right.add(idLabel);
        right.add(Box.createRigidArea(new Dimension(20,20)));
        idTextField.setMaximumSize(new Dimension(Integer.MAX_VALUE, idTextField.getPreferredSize().height) );
        right.add(idTextField);

        right.add(Box.createRigidArea(new Dimension(20,20)));
        nameLabel.setBorder(new EmptyBorder(10,10,10,10));
        right.add(nameLabel);
        right.add(Box.createRigidArea(new Dimension(20,20)));
        nameTextField.setMaximumSize(new Dimension(Integer.MAX_VALUE, nameTextField.getPreferredSize().height) );
        right.add(nameTextField);

        right.add(Box.createRigidArea(new Dimension(20,20)));
        priceLabel.setBorder(new EmptyBorder(10,10,10,10));
        right.add(priceLabel);
        right.add(Box.createRigidArea(new Dimension(20,20)));
        priceTextField.setMaximumSize(new Dimension(Integer.MAX_VALUE, priceTextField.getPreferredSize().height) );
        right.add(priceTextField);

        right.add(Box.createRigidArea(new Dimension(20,20)));
        quantityLabel.setBorder(new EmptyBorder(10,10,10,10));
        right.add(quantityLabel);
        right.add(Box.createRigidArea(new Dimension(20,20)));
        quantityTextField.setMaximumSize(new Dimension(Integer.MAX_VALUE, priceTextField.getPreferredSize().height) );
        right.add(quantityTextField);

        add(right);

        buttons.add(Box.createRigidArea(new Dimension(50, 50)));
        create.setBorder(new EmptyBorder(10, 10, 10, 10));
        buttons.add(create);

        buttons.add(Box.createRigidArea(new Dimension(50, 50)));
        update.setBorder(new EmptyBorder(10, 10, 10, 10));
        buttons.add(update);

        buttons.add(Box.createRigidArea(new Dimension(50, 50)));
        delete.setBorder(new EmptyBorder(10, 10, 10, 10));
        buttons.add(delete);

        buttons.add(Box.createRigidArea(new Dimension(50, 50)));
        exit.setBorder(new EmptyBorder(10, 10, 10, 10));
        buttons.add(exit);

        add(buttons);

        setSize(WIDTH, HEIGHT);
        setLocationRelativeTo(null);
    }

    //Get-ere pentru controller
    public String getName() { return this.nameTextField.getText(); }

    public String getPriceText() { return this.priceTextField.getText(); }

    public String getQuantityText() { return this.quantityTextField.getText(); }

    public String getIdText() { return this.idTextField.getText(); }

    public Integer getPrice() { return Integer.parseInt(this.priceTextField.getText()); }

    public Integer getQuantity() { return Integer.parseInt(this.quantityTextField.getText()); }

    public Long getId() { return Long.parseLong(this.idTextField.getText()); }

    public Long getSelectedProductId() { return (Long) productsTable.getValueAt(productsTable.getSelectedRow(),0); }

    //Refresh pentru tabel
    public void refreshProductTable(Object[][] data, String[] columnNames){
        DefaultTableModel tableModel = (DefaultTableModel) productsTable.getModel();
        tableModel.setDataVector(data,columnNames);
        tableModel.setColumnIdentifiers(columnNames);
        tableModel.fireTableDataChanged();
    }

    //Action listener
    public void addCreateActionListeners(ActionListener actionListener){
        create.addActionListener(actionListener);
    }

    public void addUpdateActionListener(ActionListener actionListener){
        update.addActionListener(actionListener);
    }

    public void addDeleteActionListener(ActionListener actionListener){
        delete.addActionListener(actionListener);
    }

    public void addExitActionListener(ActionListener actionListener){
        exit.addActionListener(actionListener);
    }
}
