package ps.Presentation.View;

import ps.Utils.Impl.ReflectionImpl;
import ps.Utils.Reflection;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionListener;

public class OrderView extends JFrame {
    private static final String TITLE = "Orders View";
    private static final int WIDTH = 700;
    private static final int HEIGHT = 600;


    private final JLabel quantityLabel;

    private final JButton order;
    private final JButton reports;
    private final JButton exit;
    private final JButton delete;


    private JTextField quantityTextField = new JTextField();

    private final JTable  ordersTable;

    private final JPanel left;
    private final JPanel right;
    private final JPanel buttons;

    private static Reflection reflection;

    public OrderView() throws HeadlessException {
        super(TITLE);

        quantityLabel= new JLabel("Quantity");

        order = new JButton("Order Product");
        reports = new JButton("Generate Reports");
        exit = new JButton("Exit");
        delete = new JButton("Delete");

        ordersTable = new JTable();

        left = new JPanel();
        right = new JPanel();
        buttons = new JPanel();

        reflection = new ReflectionImpl();

        initializeView();
        setVisible(false);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    private void initializeView() {

        left.setLayout(new BoxLayout(left, BoxLayout.PAGE_AXIS));
        right.setLayout(new BoxLayout(right,BoxLayout.PAGE_AXIS));
        buttons.setLayout(new BoxLayout(buttons,BoxLayout.PAGE_AXIS));
        setLayout(new BoxLayout(getContentPane(), BoxLayout.LINE_AXIS));


        left.add(Box.createRigidArea(new Dimension(20, 20)));
        JScrollPane jScrollPaneArticles = new JScrollPane(ordersTable, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPaneArticles.setBorder(new EmptyBorder(10, 10, 10, 10));
        left.add(jScrollPaneArticles);

        add(left);


        right.add(Box.createRigidArea(new Dimension(20,20)));
        quantityLabel.setBorder(new EmptyBorder(10,10,10,10));
        right.add(quantityLabel);
        right.add(Box.createRigidArea(new Dimension(20,20)));
        quantityTextField.setMaximumSize(new Dimension(Integer.MAX_VALUE, quantityTextField.getPreferredSize().height) );
        right.add(quantityTextField);

        add(right);

        buttons.add(Box.createRigidArea(new Dimension(50, 50)));
        order.setBorder(new EmptyBorder(10, 10, 10, 10));
        buttons.add(order);

        buttons.add(Box.createRigidArea(new Dimension(50, 50)));
        reports.setBorder(new EmptyBorder(10, 10, 10, 10));
        buttons.add(reports);

        buttons.add(Box.createRigidArea(new Dimension(50, 50)));
        exit.setBorder(new EmptyBorder(10, 10, 10, 10));
        buttons.add(exit);

        buttons.add(Box.createRigidArea(new Dimension(50, 50)));
        delete.setBorder(new EmptyBorder(10, 10, 10, 10));
        buttons.add(delete);

        add(buttons);

        setSize(WIDTH, HEIGHT);
        setLocationRelativeTo(null);
    }

    //Get-ere penrtru controller
    public String getQuantityText() { return this.quantityTextField.getText(); }

    public Integer getQuantity() { return Integer.parseInt(this.quantityTextField.getText()); }

    public JTable getOrderTable(){
        return ordersTable;
    }

    public Long getSelectedOrderId() { return (Long) this.ordersTable.getValueAt(ordersTable.getSelectedRow(),0); }

    //add action listeners
    public void addExitActionListener(ActionListener actionListener){ exit.addActionListener(actionListener); }
    public void addOrderActionListener(ActionListener actionListener) { order.addActionListener(actionListener); }
    public void addReportActionListener(ActionListener actionListener) { reports.addActionListener(actionListener); }
    public void addDeleteActionListener(ActionListener actionListener) { delete.addActionListener(actionListener); }

}
