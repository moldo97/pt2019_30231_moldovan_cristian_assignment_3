package ps.Presentation.View;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class InitView extends JFrame {
    private static final String TITLE = "Initial View";
    private static final int WIDTH = 400;
    private static final int HEIGHT = 150;
    private JButton clients = new JButton("Clients");
    private JButton products = new JButton( "Products");
    private JButton orders = new JButton( " Orders");

    public InitView() throws HeadlessException {
        super(TITLE);
        setVisible(false);
        initializeView();
        setSize(WIDTH,HEIGHT);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    private void initializeView(){
        setLayout(null);
        clients.setBounds(50,30,80,50);
        products.setBounds(150,30,100,50);
        orders.setBounds(260,30,80,50);

        add(clients);
        add(products);
        add(orders);
    }

    public void addClientsLoginActionListener(ActionListener actionListener){
        clients.addActionListener(actionListener);
    }

    public void addProductsLoginActionListener(ActionListener actionListener){
        products.addActionListener(actionListener);
    }

    public void addOrdersLoginActionListener(ActionListener actionListener){
        orders.addActionListener(actionListener);
    }

    public InitView getOuter() { return InitView.this; }
}
