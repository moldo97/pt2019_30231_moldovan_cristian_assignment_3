package ps.Utils;

import ps.Model.Client;
import ps.Model.Product;

import java.util.List;

public interface DataConverter {
    Object[][] clientToTableData(List<Client> clients);

    Object[][] productToTableData(List<Product> products);

    String[] clientToTableColumnNames();

    String[] productToTableColumnNames();
}
