package ps.Utils;

import ps.Model.Order;
import ps.Model.Product;

import java.util.List;

public interface ReportPDF {
    void exportReport(Long idClient ,List<Product> orderedProducts);
}
