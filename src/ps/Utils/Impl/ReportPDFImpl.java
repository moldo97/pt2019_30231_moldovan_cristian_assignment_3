package ps.Utils.Impl;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import ps.Model.Order;
import ps.Model.Product;
import ps.Utils.ReportPDF;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.List;

public class ReportPDFImpl implements ReportPDF {
    private static String PDF_FILE = "D:/An_3/Semestrul_2/TP_Restanta/Tema3/Report/PDF Report.pdf";

    @Override
    public void exportReport(Long idClient, List<Product> orderedProducts) {
        Document document = new Document();
        try {
            PdfWriter.getInstance(document,new FileOutputStream(PDF_FILE));
                document.open();
                document.add(new Paragraph("Ordered products"));
                document.add(new Paragraph("Client with id= " + idClient));
                orderedProducts.forEach(product -> {
                    try {
                        document.add(new Paragraph("Product name: " + product.getName() + ", price: " + product.getPrice()));
                    } catch (DocumentException e) {
                        e.printStackTrace();
                    }
                });
                document.close();
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
