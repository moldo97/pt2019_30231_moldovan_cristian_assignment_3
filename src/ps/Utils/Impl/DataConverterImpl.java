package ps.Utils.Impl;

import ps.Model.Client;
import ps.Model.Product;
import ps.Utils.DataConverter;
import java.util.List;

public class DataConverterImpl implements DataConverter {
    @Override
    public Object[][] clientToTableData(List<Client> clients) {
        Object[][] data = new Object[clients.size()][3];
        for(int row = 0 ; row < data.length ; row++){
            data[row][0] = clients.get(row).getId();
            data[row][1] = clients.get(row).getName();
            data[row][2] = clients.get(row).getAdress();
        }
        return data;
    }

    @Override
    public Object[][] productToTableData(List<Product> products) {
        Object[][] data = new Object[products.size()][4];
        for(int row = 0 ; row < data.length ; row++){
            data[row][0] = products.get(row).getId();
            data[row][1] = products.get(row).getName();
            data[row][2] = products.get(row).getPrice();
            data[row][3] = products.get(row).getQuantity();
        }
        return data;
    }

    @Override
    public String[] clientToTableColumnNames() {
        return new String[]{"Id", "Name", "Adress"};
    }

    @Override
    public String[] productToTableColumnNames() {
        return new String[]{"Id", "Name", "Price", "Quantity"};
    }
}
