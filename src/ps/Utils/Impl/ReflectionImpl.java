package ps.Utils.Impl;

import ps.Utils.Reflection;

import javax.swing.table.DefaultTableModel;
import java.lang.reflect.Field;
import java.util.List;

public class ReflectionImpl implements Reflection {

    @Override
    public <T> String[] getColumnNames(T obj) {
        Field[] fields = obj.getClass().getDeclaredFields();
        String[] columns = new String[fields.length];

        for(int i=0; i<fields.length ; i++){
            columns[i] = fields[i].getName();
        }

        return columns;
    }

    @Override
    public <T> void addRows(DefaultTableModel model, List<T> list) {
        Field[] fields = list.get(0).getClass().getDeclaredFields();

        for(int i=0 ; i < list.size() ; i++){

            int j=0;
            Object[] row = new Object[fields.length];
            for( Field field : fields)
            {
                field.setAccessible(true);
                try {
                    row[j++] = field.get(list.get(i));
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
            model.addRow(row);
        }
    }
}
