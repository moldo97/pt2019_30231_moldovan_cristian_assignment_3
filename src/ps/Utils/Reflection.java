package ps.Utils;

import javax.swing.table.DefaultTableModel;
import java.util.List;

public interface Reflection {
    <T> String[] getColumnNames(T obj);

    <T> void addRows(DefaultTableModel model, List<T> list);
}
