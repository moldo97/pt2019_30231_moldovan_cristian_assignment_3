package ps.Model;

public class Order{
    private Long id;
    private Long id_client;
    private Long id_product;
    private Integer quantity;

    public Long getId_client() {
        return id_client;
    }

    public void setId_client(Long id_client) {
        this.id_client = id_client;
    }

    public Long getId_produs() {
        return id_product;
    }

    public void setId_produs(Long id_produs) {
        this.id_product = id_produs;
    }

    public Integer getCantitate() {
        return quantity;
    }

    public void setCantitate(Integer cantitate) {
        this.quantity = cantitate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
