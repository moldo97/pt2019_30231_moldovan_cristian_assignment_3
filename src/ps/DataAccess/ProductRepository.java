package ps.DataAccess;

import ps.Model.Product;

import java.util.List;

public interface ProductRepository {
    Product findById(Long id);

    List<Product> findAll();

    Product create(Product product);

    Product update(Product product);

    boolean deleteById(Long id);
}
