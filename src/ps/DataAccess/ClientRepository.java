package ps.DataAccess;

import ps.Model.Client;

import java.util.List;

public interface ClientRepository {
    Client findById(Long id);

    List<Client> findAll();

    Client create(Client client);

    Client update(Client client);

    boolean deleteById(Long id);
}
