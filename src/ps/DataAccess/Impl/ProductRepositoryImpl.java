package ps.DataAccess.Impl;

import ps.Model.Product;
import ps.DataAccess.ProductRepository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProductRepositoryImpl implements ProductRepository {
    private final JDBConnectionWrapper jdbConnectionWrapper;

    public ProductRepositoryImpl(JDBConnectionWrapper jdbConnectionWrapper) {
        this.jdbConnectionWrapper = jdbConnectionWrapper;
    }

    @Override
    public Product findById(Long id) {
        Connection connection = jdbConnectionWrapper.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * from product WHERE id= ?");
            preparedStatement.setLong(1,id);

            ResultSet resultSet = preparedStatement.executeQuery();

            if(resultSet.next()){
                Product product = new Product();
                product.setId(resultSet.getLong(1));
                product.setName(resultSet.getString(2));
                product.setPrice(resultSet.getInt(3));
                product.setQuantity(resultSet.getInt(4));

                return product;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Product> findAll() {
        Connection connection = jdbConnectionWrapper.getConnection();
        List<Product> products = new ArrayList<>();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * from product");

            ResultSet resultSet = preparedStatement.executeQuery();

            while(resultSet.next()){
                Product product = new Product();
                product.setId(resultSet.getLong(1));
                product.setName(resultSet.getString(2));
                product.setPrice(resultSet.getInt(3));
                product.setQuantity(resultSet.getInt(4));

                products.add(product);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return products;
    }

    @Override
    public Product create(Product product) {
        Connection connection = jdbConnectionWrapper.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO product (name,price,quantity) VALUES(?,?,?)", Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1,product.getName());
            preparedStatement.setInt(2,product.getPrice());
            preparedStatement.setInt(3,product.getQuantity());

            preparedStatement.execute();

            ResultSet resultSet = preparedStatement.getGeneratedKeys();

            if(resultSet.next()){
                product.setId(resultSet.getLong(1));
                return product;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Product update(Product product) {
        Connection connection = jdbConnectionWrapper.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("UPDATE product SET name= ?, price= ?, quantity= ? WHERE id= ?",Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1,product.getName());
            preparedStatement.setInt(2,product.getPrice());
            preparedStatement.setInt(3,product.getQuantity());
            preparedStatement.setLong(4,product.getId());

            int changedRows = preparedStatement.executeUpdate();

            if(changedRows > 0){
                return product;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean deleteById(Long id) {
        Connection connection = jdbConnectionWrapper.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("DELETE from product WHERE id= ?");
            preparedStatement.setLong(1,id);

            int changedRows = preparedStatement.executeUpdate();

            return changedRows > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}
