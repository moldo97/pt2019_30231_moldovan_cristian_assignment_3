package ps.DataAccess.Impl;

import ps.Model.Order;

import ps.DataAccess.OrderRepository;
import ps.Utils.ReportPDF;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class OrderRepositoryImpl implements OrderRepository {
    private final JDBConnectionWrapper jdbConnectionWrapper;

    public OrderRepositoryImpl(JDBConnectionWrapper jdbConnectionWrapper) {
        this.jdbConnectionWrapper = jdbConnectionWrapper;
    }

    @Override
    public Order create(Order order) {
        Connection connection = jdbConnectionWrapper.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO order1 (idClient,idProduct,quantity) VALUES(?,?,?)", Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setLong(1,order.getId_client());
            preparedStatement.setLong(2,order.getId_produs());
            preparedStatement.setInt(3,order.getCantitate());

            preparedStatement.execute();

            ResultSet resultSet = preparedStatement.getGeneratedKeys();

            if(resultSet.next()){
                order.setId(resultSet.getLong(1));
                return order;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Order> findAll() {
        Connection connection = jdbConnectionWrapper.getConnection();
        List<Order> orders = new ArrayList<>();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * from order1");

            ResultSet resultSet = preparedStatement.executeQuery();

            while(resultSet.next()){
                Order order = new Order();

                order.setId(resultSet.getLong(1));
                order.setId_client(resultSet.getLong(2));
                order.setId_produs(resultSet.getLong(3));
                order.setCantitate(resultSet.getInt(4));

                orders.add(order);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return orders;
    }

    @Override
    public Order findById(Long id) {
        Connection connection = jdbConnectionWrapper.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * from order1 where id= ?");
            preparedStatement.setLong(1,id);

            ResultSet resultSet = preparedStatement.executeQuery();

            if(resultSet.next()){
                Order order = new Order();

                order.setId(resultSet.getLong(1));
                order.setId_client(resultSet.getLong(2));
                order.setId_produs(resultSet.getLong(3));
                order.setCantitate(resultSet.getInt(4));

                return order;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean deleteById(Long id) {
        Connection connection = jdbConnectionWrapper.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("DELETE from order1 where id= ?");
            preparedStatement.setLong(1,id);

            int changedRows = preparedStatement.executeUpdate();

            return changedRows > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}
