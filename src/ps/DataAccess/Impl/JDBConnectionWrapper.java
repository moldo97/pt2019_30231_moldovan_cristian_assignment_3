package ps.DataAccess.Impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class JDBConnectionWrapper {
    private static final String DB_URL = "jdbc:mysql://localhost:3306/";
    private static final String USER = "root";
    private static final String PASS = "";

    private Connection connection;

    public JDBConnectionWrapper(String schemaName) {
        try {
            connection = DriverManager.getConnection(DB_URL + schemaName + "?useSSL=false", USER, PASS);
            createTables();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void createTables() throws SQLException {
        Statement statement = connection.createStatement();

        String sql = "CREATE TABLE IF NOT EXISTS client (" +
                "  id BIGINT(100) NOT NULL AUTO_INCREMENT," +
                "  name varchar(255) NOT NULL," +
                "  adress varchar(255) NOT NULL," +
                "  PRIMARY KEY (id)," +
                "  UNIQUE KEY id_UNIQUE (id)" +
                ") ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;";
        statement.execute(sql);

        sql = "CREATE TABLE IF NOT EXISTS product (" +
                "  id BIGINT(100) NOT NULL AUTO_INCREMENT," +
                "  name varchar(255) NOT NULL," +
                "  price INT(100) NOT NULL," +
                "  quantity INT(100) NOT NULL," +
                "  PRIMARY KEY (id)," +
                "  UNIQUE KEY id_UNIQUE (id)" +
                ") ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;";
        statement.execute(sql);

        sql = "CREATE TABLE IF NOT EXISTS order1 (" +
                "  id BIGINT(100) NOT NULL AUTO_INCREMENT," +
                "  idClient BIGINT(100) NOT NULL," +
                "  idProduct BIGINT(100) NOT NULL," +
                "  quantity INT(100) NOT NULL," +
                "  FOREIGN KEY (idClient) REFERENCES client(id)," +
                "  FOREIGN KEY (idProduct) REFERENCES product(id)," +
                "  PRIMARY KEY (id)," +
                "  UNIQUE KEY id_UNIQUE (id)" +
                ") ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;";
        statement.execute(sql);

    }

    public Connection getConnection() {
        return connection;
    }
}
