package ps.DataAccess.Impl;

import ps.Model.Client;

import ps.DataAccess.ClientRepository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ClientRepositoryImpl implements ClientRepository {
    private final JDBConnectionWrapper jdbConnectionWrapper;

    public ClientRepositoryImpl(JDBConnectionWrapper jdbConnectionWrapper) {
        this.jdbConnectionWrapper = jdbConnectionWrapper;
    }

    @Override
    public Client findById(Long id) {
        Connection connection = jdbConnectionWrapper.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * from client WHERE id= ?");
            preparedStatement.setLong(1,id);

            ResultSet resultSet = preparedStatement.executeQuery();

            if(resultSet.next()){
                Client client = new Client();

                client.setId(resultSet.getLong(1));
                client.setName(resultSet.getString(2));
                client.setAdress(resultSet.getString(3));

                return client;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Client> findAll() {
        Connection connection = jdbConnectionWrapper.getConnection();
        List<Client> clients = new ArrayList<>();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * from client");

            ResultSet resultSet = preparedStatement.executeQuery();

            while(resultSet.next()){
                Client client = new Client();

                client.setId(resultSet.getLong(1));
                client.setName(resultSet.getString(2));
                client.setAdress(resultSet.getString(3));

                clients.add(client);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return clients;
    }

    @Override
    public Client create(Client client) {
        Connection connection = jdbConnectionWrapper.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO client (name,adress) VALUES(?,?)", Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1,client.getName());
            preparedStatement.setString(2,client.getAdress());

            preparedStatement.execute();

            ResultSet resultSet = preparedStatement.getGeneratedKeys();

            if(resultSet.next()){
                client.setId(resultSet.getLong(1));
                return client;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Client update(Client client) {
        Connection connection = jdbConnectionWrapper.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("UPDATE client SET name= ?, adress= ? WHERE id= ?",Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1,client.getName());
            preparedStatement.setString(2,client.getAdress());
            preparedStatement.setLong(3,client.getId());

            int changedRows = preparedStatement.executeUpdate();

            if(changedRows > 0){
                return client;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean deleteById(Long id) {
        Connection connection = jdbConnectionWrapper.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("DELETE from client WHERE id= ?");
            preparedStatement.setLong(1,id);

            int changedRows = preparedStatement.executeUpdate();

            return changedRows > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}
