package ps.DataAccess;

import ps.Model.Order;

import java.util.List;

public interface OrderRepository {
    Order create(Order order);

    List<Order> findAll();

    Order findById(Long id);

    boolean deleteById(Long id);
}
