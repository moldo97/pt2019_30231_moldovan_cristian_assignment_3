package ps;

import ps.Presentation.View.ClientView;
import ps.Presentation.View.ProductView;
import ps.BusinessLogic.ClientService;
import ps.BusinessLogic.Impl.ClientServiceImpl;
import ps.BusinessLogic.Impl.OrderServiceImpl;
import ps.BusinessLogic.Impl.ProductServiceImpl;
import ps.BusinessLogic.OrderService;
import ps.BusinessLogic.ProductService;
import ps.DataAccess.ClientRepository;
import ps.DataAccess.Impl.ClientRepositoryImpl;
import ps.DataAccess.Impl.JDBConnectionWrapper;
import ps.DataAccess.Impl.OrderRepositoryImpl;
import ps.DataAccess.Impl.ProductRepositoryImpl;
import ps.DataAccess.OrderRepository;
import ps.DataAccess.ProductRepository;
import ps.Presentation.Controller.ClientController;
import ps.Presentation.Controller.InitController;
import ps.Presentation.Controller.OrderController;
import ps.Presentation.Controller.ProductController;
import ps.Presentation.View.InitView;
import ps.Presentation.View.OrderView;
import ps.Utils.DataConverter;
import ps.Utils.Impl.DataConverterImpl;
import ps.Utils.Impl.ReflectionImpl;
import ps.Utils.Reflection;

public class Main {
    private static final String SCHEMA_NAME = "ordermanagement";
    private static DataConverter dataConverter;

    private static InitView initView;
    private static ProductView productView;
    private static OrderView orderView;
    private static ClientView clientView;

    private static InitController initController;
    private static ProductController productController;
    private static OrderController orderController;
    private static ClientController clientController;

    private static ClientRepository clientRepository;
    private static ProductRepository productRepository;
    private static OrderRepository orderRepository;

    private static ClientService clientService;
    private static ProductService productService;
    private static OrderService orderService;

    private static Reflection reflection;

    public static void main(String[] args) {
        JDBConnectionWrapper jdbConnectionWrapper = new JDBConnectionWrapper(SCHEMA_NAME);

        dataConverter = new DataConverterImpl();
        reflection = new ReflectionImpl();

        //initialize repository
        clientRepository = new ClientRepositoryImpl(jdbConnectionWrapper);
        productRepository = new ProductRepositoryImpl(jdbConnectionWrapper);
        orderRepository = new OrderRepositoryImpl(jdbConnectionWrapper);

        //initialize service
        clientService = new ClientServiceImpl(clientRepository);
        productService = new ProductServiceImpl(productRepository);
        orderService = new OrderServiceImpl(orderRepository);

        //initialize view
        initView = new InitView();
        productView = new ProductView();
        orderView = new OrderView();
        clientView = new ClientView();

        openInit();
    }

    public static void openInit(){
        //deschidem controller init
        initController = new InitController(initView);
    }

    public static void openClientView(){
        //deschidem controller client
        clientController = new ClientController(clientView,dataConverter,clientService);
    }

    public static void openProductView(){
        //deschidem controller product
        productController = new ProductController(productView,dataConverter,productService);
    }

    public static void openOrderView(){
        //deschidem controller order
        orderController = new OrderController(orderView,productView,clientView,orderService,productService,reflection);
    }
}
